///
/// @file 
/// @ingroup ix_group
/// @brief Implements a command-line interface for runnning tests.
///
#include <sstream>
#include <fstream>

#include "UnixUtils.H"
#include "ComLine.H"
#include "FDUtils.H"

#include "RunTest.H"

namespace ix
{
  int RunTest(int argc,char *argv[])
  {
    
    RTSComLine comline((const char **)(argv));
    // The call to comline.Initialize() reads the command line arguments
    // from the array passed in the previous line.
    comline.Initialize();
    // The ProcessOptions() call does detailed examination of the command
    // line arguments to check for user errors or other problems. This call
    // will return non-zero if there were errors on the commandline.
    int clerr = comline.ProcessOptions();
    // Check if the user just wanted to get the help and exit
    if(!comline.GetOption("help").empty()){
      // Print out the "long usage" (i.e. help) message to stdout
      std::cout << comline.LongUsage() << std::endl;
      return(0);
    }
    if(clerr){
      std::cout << comline.ErrorReport() << std::endl
                << std::endl << comline.ShortUsage() << std::endl;
      return(1);
    }
    // These outstreams allow the output to file to be set up and separated
    // from the stdout.
    //    std::ofstream Ouf;
    //    std::ostream *Out = &std::cout;

    // The next few lines populate some strings based on the 
    // users input from the commandline.
    std::string OutFileName(comline.GetOption("output"));
    //    std::string TestName(comline.GetOption("name"));
    std::string ListName(comline.GetOption("list"));
    std::string FileName(comline.GetOption("file"));
    std::string sverb(comline.GetOption("verblevel"));
    std::string HostName(comline.GetOption("hostname"));
    std::string PlatformsName(comline.GetOption("platforms"));
    std::string SourceDirectory(comline.GetOption("srcdir"));
    std::string BinaryDirectory(comline.GetOption("bindir"));
    std::string ScriptArgument(comline.GetOption("argument"));
    bool keepFiles = !comline.GetOption("keep").empty();
    
    if(PlatformsName.empty() && ListName.empty() && FileName.empty()){
      std::cout  << comline.ShortUsage() << std::endl
                 << "ix::RunTest> Error: One of --list or --platform or --file"
                 << " must be specified. Exiting (fail)." << std::endl;
      return(1);
    }
    if((!PlatformsName.empty() && !ListName.empty())||
       (!FileName.empty() && (!PlatformsName.empty() || !ListName.empty()))){
      std::cout << comline.ShortUsage() << std::endl;
      std::cout << "ix::RunTest> Error: Ambigous settings. --list, --platform, and"
                << " --file are mutually exclusive. Exiting (fail)." << std::endl;
      return(1);
    }
    if(!HostName.empty() && PlatformsName.empty()){
      std::cout << "ix::RunTest> Warning: Hostname option is meaningless without platforms file."
                << std::endl;
    }
    // The following block parses and sets the verbosity level
    int verblevel = 1;
    if(!sverb.empty()){
      verblevel = 1;
      if(sverb != ".true."){
        std::istringstream Istr(sverb);
        Istr >> verblevel;
        if(verblevel < 0)
          verblevel = 1;
      }
    }
    if(verblevel > 2)
      std::cout << "ix::RunTest> Starting up with the following environment:" << std::endl
                << "ix::RunTest> OutFileName = " << OutFileName << std::endl
                << "ix::RunTest> ListName = " << ListName << std::endl
                << "ix::RunTest> FileName = " << FileName << std::endl
                << "ix::RunTest> HostName = " << HostName << std::endl
                << "ix::RunTest> PlatformsName = " << PlatformsName << std::endl
                << "ix::RunTest> SourceDirectory = " << SourceDirectory << std::endl
                << "ix::RunTest> BinaryDirectory = " << BinaryDirectory << std::endl;
    
    // This block sets up the output file if the user specified one
    bool keep_outfile = false;
    if(!OutFileName.empty()){
      keep_outfile = true;
      //      Ouf.open(OutFileName.c_str());
      //      if(!Ouf){
      //        std::cout << "ix::RunTests> Error: Could not open output file, " 
      //                  <<  OutFileName << " for test output. Exiting (fail)." << std::endl;
      //        return(1);
      //      }
      //      Out = &Ouf;
    } else {
      std::string name_stub("test_script.o");
      ix::sys::TempFileName(name_stub);
      OutFileName = name_stub;
      if(verblevel > 2)
        std::cout << "ix::RunTest> Setting temp output file name to (" << OutFileName << ")" << std::endl;
    }
    
    if(HostName.empty()){
      HostName = ix::sys::Hostname();
      if(verblevel > 2){
	std::cout << "ix::RunTest> Found HostName = " << HostName << std::endl;
      }
    }
    //    if(PlatformsName.empty())
    //      PlatformsName = "./share/platforms/parallel_platforms";
   
    std::string BasePath;
    if(!PlatformsName.empty()){
      if(verblevel > 2)
        std::cout << "ix::RunTest> Processing Platform = (" << PlatformsName << ")" << std::endl;
      // Open up the platforms file and find the host
      std::ifstream PlatInf;
      std::string::size_type x = PlatformsName.find_last_of("/");
      if(x != std::string::npos)
        BasePath.assign(PlatformsName.substr(0,x+1));
      PlatInf.open(PlatformsName.c_str());
      if(!PlatInf){
        std::cout << "ix::RunTest> Error: Could not open platforms file, "
                  << PlatformsName << " for resolving platform-specific tests for "
                  << HostName << ". Exiting (fail)." << std::endl;
        return(1);
      } else {
        if(verblevel > 2)
        std::cout << "ix::RunTest> BasePath = (" << BasePath << ")" << std::endl;
      }
      //      std::string PlatformFileName;
      std::string splatform;
      std::string defaultRunningName;
      while(std::getline(PlatInf,splatform) && ListName.empty()){
        std::istringstream Istr(splatform);
        std::string platform_name;
        std::string running_name;
        Istr >> platform_name >> running_name;
	if(platform_name == "DEFAULT")
	  defaultRunningName = running_name;
        if(HostName == platform_name){
          ListName.assign(running_name);
	} else if(HostName.find(platform_name) != std::string::npos){
	  ListName.assign(running_name);
	}
      }
      if(ListName.empty()){
        std::cout << "ix::RunTest> Warning: Could not find a " << HostName 
		  << "-specific configuration." << std::endl;
	if(defaultRunningName.empty()){
	  std::cout << "ix::RunText> Error: No default platform, cannot continue."
		    << std::endl;
	  return(1);
	} else {
	  std::cout << "ix::RunTest> Warning: Setting default configuration (" 
		    << defaultRunningName << ")." << std::endl;
	  ListName.assign(defaultRunningName);
	}
      } else {
        if(verblevel > 2)
          std::cout << "ix::RunTest> Found ListName = (" << ListName << ")" << std::endl;
      }
    } else {
      if(verblevel > 2)
        std::cout << "ix::RunTest> No platform name given." << std::endl;
    }

    // Now we either have a list, or a single named test.
    std::vector<std::string> TestFileNames;
    if(!ListName.empty()){
      // Support both relative and absolute path
      std::ifstream ListInf;
      ListInf.open(ListName.c_str());
      if(!ListInf){
	std::string RelName;
        if(!BasePath.empty()){
	  RelName.assign(BasePath+ListName);
          ListInf.open(RelName.c_str());
        }
        if(!ListInf){
          std::cout << "ix::RunTest> Error: Could not open list file, "
                    << ListName << ".";
	  if(!RelName.empty()){
	    std::cout << " Tried relative path (" << RelName << ").";
	  }
	  std::cout << " Exiting (fail)." << std::endl;
          return(1);
        }
      }
      if(BasePath.empty()){
        std::string::size_type x = ListName.find_last_of("/");
        if(x != std::string::npos){
          BasePath.assign(ListName.substr(0,x+1));
          if(verblevel > 2)
            std::cout << "ix::RunTest> Assigning BasePath = (" << BasePath << ")" << std::endl;
        } 
      }
      std::string raw_test_name;
      while(std::getline(ListInf,raw_test_name)){
        if(verblevel > 2)
          std::cout << "ix::RunTest> Found listed test = (" << raw_test_name << ")" << std::endl;
        if(ix::sys::FILEEXISTS(raw_test_name) && ix::sys::ISLINK(raw_test_name))
          ix::sys::Remove(raw_test_name);
        if(!ix::sys::FILEEXISTS(raw_test_name)){
          std::string RelName(BasePath+raw_test_name);
          if(!ix::sys::FILEEXISTS(RelName)){
            std::cout << "ix::RunTest> Warning: Could not find test " 
                      << raw_test_name << " from " << ListName << "." 
                      << std::endl;
          } else {
            TestFileNames.push_back(RelName);
            if(verblevel > 2)
              std::cout << "ix::RunTest> Renamed test = (" << RelName << ")" << std::endl;
          }
        } else {
          std::cout << "ix::RunTest> Warning: Using raw test file name (" << raw_test_name << ")." << std::endl;
          TestFileNames.push_back(raw_test_name);
        }
      }
      ListInf.close();
    } else { // An explicit filename must have been given
      if(verblevel > 2)
        std::cout << "ix::RunTest> Processing explicit test file = (" << FileName << ")" << std::endl;
      if(!ix::sys::FILEEXISTS(FileName)){
        std::cout << "ix::RunTest> Error: Could not find specified test, "
                  << FileName << ". Exiting (fail)." << std::endl;
        return(1);
      }
      TestFileNames.push_back(FileName);
    }
    if(verblevel > 2)
      std::cout << "ix::RunTest> Processing tests ... " << std::endl;
    // Now we have a list of full paths to tests to run
    int err = 0;
    std::vector<std::string>::iterator ti = TestFileNames.begin();
    while(ti != TestFileNames.end()){
      bool madeLink = false;
      // The following block simply makes a link to the testing
      // script in the current directory for running.
      std::string TestPath(*ti++);
      std::string TestName(TestPath);
      if(verblevel > 2)
        std::cout << "ix::RunTest> Processing Test = (" << TestPath << ")" << std::endl;

      std::string::size_type x = TestName.find_last_of("/");
      if(x != std::string::npos) {
        TestName = TestName.substr(x+1);
      }
      std::string LocalTest("./"+TestName);
      if(verblevel > 2)
        std::cout << "ix::RunTest> Local test name = (" << LocalTest << ")" << std::endl;
      if(ix::sys::FILEEXISTS(LocalTest)){
        const std::string existingLinkPath(ix::sys::ResolveLink(LocalTest));
        if(existingLinkPath != TestPath){
          if(verblevel > 2){
            std::cout << "ix::RunTest> Existing local test executable: " << LocalTest
                      << std::endl;
          }
          ix::sys::Remove(LocalTest);
        }
      }
      if(!ix::sys::FILEEXISTS(LocalTest)){
        if(TestPath == TestName){
          std::cout << "ix::RunTest> Warning: Using local (non-link) path to testing file." << std::endl;
        } else {
          if(verblevel > 1)
            std::cout << "ix::RunTest> Linking " << TestPath << " to " << LocalTest << std::endl;
          if(ix::sys::SymLink(TestPath,LocalTest)){
            //            err++;
            std::cout << "ix::RunTest> Warning: Could not create symbolic link "
                      << "to " << TestPath << " at " << LocalTest << "." 
                      << std::endl;
          } else {
            madeLink = true;
          }
        }
      }
      if(verblevel > 1)
        std::cout << "ix::RunTest> Running " << TestName << "." << std::endl;
      std::string TmpComOut(ix::sys::TempFileName(OutFileName));
      std::string Command(LocalTest+" "+TmpComOut);
      if(!SourceDirectory.empty())
        Command = Command + " " + SourceDirectory;
      if(!BinaryDirectory.empty())
        Command = Command + " " + BinaryDirectory;
      if(!ScriptArgument.empty())
        Command = Command + " " + ScriptArgument;
      if(verblevel > 1)
        std::cout << "ix::RunTest> Running command: " << Command << std::endl;
      ix::sys::InProcess TestProcess(Command);
      std::string testoutline;
      while(std::getline(TestProcess,testoutline)){
        if(verblevel)
          std::cout << testoutline << std::endl;
      }
      if(!keepFiles){
	ix::sys::Remove(LocalTest);
      }
      std::ifstream ComInf(TmpComOut.c_str());
      if(!ComInf){
        std::cout << "ix::RunTest> Warning: Cannot access test results from "
                  << TestName << ". Continuing." << std::endl;
        err++;
      } else {
        std::ofstream Ouf;
        Ouf.open(OutFileName.c_str(),std::ios::app);
        if(!Ouf){
          std::cout << "ix::RunTest> Error: Cannot open outfile, " << OutFileName
                    << ". Exiting (fail)." << std::endl;
          return(1);
        }
        std::string comline;
        while(std::getline(ComInf,comline))
          Ouf << comline << std::endl;
        ComInf.close();
        Ouf.close();
	if(!keepFiles)
	  ix::sys::Remove(TmpComOut);
        if(madeLink){
          // Let's remove the link we made and see if it helps squish spurious failures
          ix::sys::Remove(LocalTest);
        }
      }
    }
    if(err && (verblevel > 1))
      std::cout << "ix::RunTest> Warning: " << err << " errors occurred during testing."
                << std::endl;
    if(!keep_outfile){
      std::ifstream Inf;
      Inf.open(OutFileName.c_str());
      if(!Inf){
        std::cout << "ix::RunTest> Error: Could not open test output"
                  << ". Test must have gone wrong.  Exiting (fail)."
                  << std::endl;
        return(1);
      }
      std::string line;
      while(std::getline(Inf,line))
        std::cout << line << std::endl;
      Inf.close();
      if(!keepFiles)
	ix::sys::Remove(OutFileName);
    }
    return(err);
  }
};


int main(int argc,char *argv[])
{
  return(ix::RunTest(argc,argv));
}
