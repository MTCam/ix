#!/bin/tcsh

set outFile=${1}
set binDir=${3}

rm -f tmpresults_2.txt
cat <<EOF > ./codelet3_parallel_test_batch.csh
#!/bin/tcsh
#
#PBS -l nodes=2:ppn=8
#PBS -l walltime=00:30:00 
#PBS -j oe
#PBS -o codelet3_parallel_test_batch_output
##PBS -A XPACC_TEST

# cd \${PBS_O_WORKDIR}
mpiexec -np 16 ${binDir}/pepi -o tmpresults_2.txt 1000000
EOF
qsub -cwd codelet3_parallel_test_batch.csh
@ i = 1
while($i <= 720)
    @ i += 1
    if( -e tmpresults_2.txt ) then
        @ i += 721;
    else
        sleep 5;
    endif
end
printf "PEPI:Runs=" >> ${outFile}
@ err = 0
if ( -e tmpresults_2.txt ) then
   printf "1\n" >> ${outFile}
else
   printf "0\n" >> ${outFile}
   @ err += 1
endif
set RESULTS=`cat tmpresults_2.txt | grep 3.141592653589`
printf "PEPI:Works=" >> $1
if ( "$RESULTS" == "") then
   printf "0\n" >> ${outFile}
   @ err += 1
else
   printf "1\n" >> ${outFile}
endif  
rm -f tmpresults_2.txt
rm -f ./codelet3_parallel_test_batch.csh
rm -f codelet3_parallel_test_batch_output
exit ${err}
