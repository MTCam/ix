///
/// @file
/// @ingroup ix_group ix_app
/// @brief Application definitions for parallel applications
/// @author Michael T. Campbell (mtcampbe@illinois.edu)
/// @date
///
#ifndef __PARALLEL_APPLICATION_H__
#define __PARALLEL_APPLICATION_H__

#include "Application.H"
#include "COMM.H"
#include <cmath>

namespace ix { namespace app {

    typedef ix::comm::CommunicatorObject CommunicatorType;
    typedef ix::global::ParallelGlobalObj<CommunicatorType,std::string,int,ProfilerType> ParallelGlobalType;
    typedef ix::global::Program<ParallelGlobalType,CommandLineType> ParallelProgramType;

    class parallel_program : public ParallelProgramType
    {
    protected:
      /// Name of file for output.
      std::string outFileName;
      /// Output file stream object for output.
      std::ofstream *outFilePtr;
    public:
      ///
      /// Default constructor.
      ///
      parallel_program() :
	ParallelProgramType()
      {
	verblevel = 0;
	outFilePtr = NULL;
      };
      ///
      /// Constructor designed to take all commandline args.
      ///
      parallel_program(int nargs,char **args) :
	ParallelProgramType(nargs,args)
      {
	verblevel = 0;
	outFilePtr = NULL;
      };
      ///
      /// Constructor designed to take all commandline args.
      ///
      parallel_program(CommandLineType &comline,CommunicatorType &incomm)
      {
	this->_command_line.Copy(comline);
	this->Init(_command_line.ProgramName(),incomm);
	verblevel = 0;
	outFilePtr = NULL;
      };

      ///
      /// Populates native data structures from commandline.
      ///
      virtual int Initialize()
      {
	if(ParallelProgramType::Initialize()){
	  ErrOut("Failed to initialize.\n");
	  ErrOut(CommandLine().ErrorReport()+std::string("\n"));
	  ErrOut("Use -h,--help for usage help.\n\n");
	  ErrOut(CommandLine().ShortUsage()+std::string("\n"));
	  return(1);
	}
	if(!CommandLine().GetOption('h').empty()){
	  StdOut(CommandLine().LongUsage());
	  return(1);
	}
	std::string applicationName(CommandLine().ProgramName());

	// Verbosity level processing intent:
	// 0 - be silent
	// 1 - normal verbosity
	// 2 - increased verbosity
	// 3 - debug mode engaged (debug stream engaged for root process)
	// 4+ - debug mode (with debugging logged on all processors)
	std::string sverb(CommandLine().GetOption('v'));
	if(!sverb.empty()){
	  if(sverb == ".true.")
	    SetVerbLevel(1);
	  else {
	    int rank = Rank();
	    int nproc = NProc();
	    int nZeros = std::log10((double)nproc);
	    std::istringstream Istr(sverb);
	    int verbLevel;
	    Istr >> verbLevel;
	    SetVerbLevel(verbLevel);
	    if(verbLevel > 2){
	      if(rank > 0){
		outFilePtr = new std::ofstream;
		std::ostringstream outFileNameStream;
		outFileNameStream << applicationName << "_";
		for(int i = nZeros;i > 0;i--){
		  int numRanks = std::pow((double)10,(double)i);
		  if(rank < numRanks)
		    outFileNameStream << "0";
		}
		if(!ErrStreamReady())
		  SetErrStream(std::cerr);
		outFileNameStream << rank;
		outFileName.assign(outFileNameStream.str());
		outFilePtr->open(outFileNameStream.str().c_str());
		if(!(*outFilePtr)){
		  globalCommunicator.SetErr(1);
		}
	      }
	      if(globalCommunicator.Check()){
		if(!rank)
		  ErrOut("Unable to open output on all processors.\n");
		return(1);
	      } else if(rank > 0) {
		SetOutStream(*outFilePtr);
	      }
	      SetDebugStream(OutStream());
	      SetDebugLevel(verbLevel - 2);
	    }
	  }
	}
	// Process profiling control
	std::string sprof(CommandLine().GetOption('p'));
	Profiling(false);
	DisableProfilingBarriers();
	if(!sprof.empty()){
	  if(sprof == ".true.")
	    Profiling(true);
	  else {
	    std::istringstream Istr(sprof);
	    int profLevel;
	    Istr >> profLevel;
	    if(profLevel == 0)
	      Profiling(false);
	    if(profLevel >= 1){
	      Profiling(true);
	      if(profLevel > 1)
		EnableProfilingBarriers();
	    }
	  }
	}
	// Process profiling control
	std::string sdebug(CommandLine().GetOption('d'));
	if(!sdebug.empty()){
	  if(sdebug == ".true.")
	    SetDebugLevel(1);
	  else {
	    std::istringstream Istr(sdebug);
	    int debugLevel;
	    Istr >> debugLevel;
	    if(debugLevel >= 1)
	      SetDebugLevel(debugLevel);
	  }
	}
	return(0);
      };


      ///
      /// Destructor.
      ///
      virtual ~parallel_program() {
	if(outFilePtr){
	  outFilePtr->close();
	  delete outFilePtr;
	}
	if(Rank() > 0)
	  UnsetOutStream();
	SetOutStream(std::cout);
      };

      ///
      /// This function implements the main function executed by the program.
      /// 
      virtual int Run(){};
      virtual int Finalize() {
	if(OutStreamReady())
	  Report(OutStream());
	if(ParallelProgramType::Finalize()){
	  ErrOut("Failed to finalize.\n");
	  if(ErrStreamReady())
	    DumpErrors(ErrStream());
	  return(1);
	}
	return(0);
      };
    };
  }
}

#endif
