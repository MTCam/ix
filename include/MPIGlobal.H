///
/// Defines MPI-specific parallel global and program classes
///
#ifndef __MPI_GLOBAL_H__
#define __MPI_GLOBAL_H__
#include "Global.H"
#include "COMM.H"

namespace ix { 
  namespace global {
    
    typedef comm::CommunicatorObject CommunicatorType;
    typedef profiler::ProfilerObj ProfilerType;
    typedef ParallelGlobalObj<CommunicatorType,std::string,int,ProfilerType> parallelglobal;
    
    
    class mpiglobal : public parallelglobal
    {

    public:
      mpiglobal() :
	parallelglobal()
      {
	profilingBarrier = false;
      };
      
      mpiglobal(parallelglobal &inglob) :
	parallelglobal(inglob)
      {
	profilingBarrier = false;
      };
      
      mpiglobal(mpiglobal &pglobin) :
	parallelglobal(pglobin)
      {
	globalCommunicator.Initialize(pglobin.globalCommunicator);
	profilingBarrier = false;
	
      };
      mpiglobal(const std::string &name) :
	parallelglobal(name)
      {
	//      Init(name);
	profilingBarrier = false;	
      };
      mpiglobal(const std::string &name,unsigned int id) :
	parallelglobal(name,id)
      {
	//      Init(name);
	profilingBarrier = false; 
      };
      mpiglobal(int narg,char **args) :
	parallelglobal(narg,args)
      {
	int result = Init(narg,args);
	assert(result == 0);
	profilingBarrier = false;
	
      };
      mpiglobal(int narg,char **args,CommunicatorType &incomm) :
        parallelglobal(narg,args)
      {
	int result = Init(util::stripdirs(args[0]),incomm);
	assert(result == 0);
	profilingBarrier = false;
      };
      mpiglobal(int narg,char **args,MPI_Comm &incomm) :
        parallelglobal(narg,args)
      {
	ix::comm::CommunicatorObject myComm(incomm);
	int result = parallelglobal::Init(util::stripdirs(args[0]),myComm);
	assert(result == 0);
	profilingBarrier = false;
      };
      int NumProc()
      {
	return(globalCommunicator.Size());
      };
    };
    
    
    template<typename ComLineType>
    class mpiprogram : public mpiglobal
    {
    protected:
      ComLineType _command_line;
    public:
      mpiprogram() :
	mpiglobal()
      {};
      mpiprogram(mpiglobal &inglob) :
	mpiglobal(inglob){};
      mpiprogram(ComLineType &incom) :
	mpiglobal(),_command_line(incom)
      {};
      mpiprogram(int narg,char **args) :
	mpiglobal(narg,args)
      {
	_command_line.Record((const char **)args);
	//      SetName(_command_line.ProgramName());
      };
      mpiprogram(ComLineType &incom,mpiglobal &inglob) :
	mpiglobal(inglob), _command_line(incom) {};
      mpiprogram(int narg,char **args,mpiglobal &inglob) :
	mpiglobal(inglob)
      {
	_command_line.Record((const char **)args);
	//      SetName(_command_line.ProgramName());
      };
      mpiprogram(int narg,char **args,MPI_Comm &inComm) :
	mpiglobal(narg,args,inComm) 
      {
	_command_line.Record((const char **)args);
      };
      virtual inline int Initialize()
      {
	_command_line.Initialize();
	return(_command_line.ProcessOptions());
      };
      ComLineType &CommandLine() {return _command_line; };
      virtual inline int Run(){return(0);};
      virtual inline int Finalize(){return(mpiglobal::Finalize());};
      virtual ~mpiprogram(){};
    };
    
  }
}  
#endif
