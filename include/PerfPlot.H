#include "AppTools.H"
#include <iostream>

typedef apptools::parameters ConfigType;
typedef apptools::comlinehandler CommandLineType;
using apptools::OpenFile;

struct gpconfig {
  std::string gpDisplayTerm;
  std::string gpFileTerm;
  std::string gpFileName;
  std::string gpCustomConfig;
};
  
struct timersettings {
  double importantLimit;
  bool weakMode;
  int dataColumn;
  int plotColumn;
  bool gridMode;
  bool effMode;
  bool comparisonMode;
  std::string platformName;
  std::string applicationName;
  std::string totalTimer;
  std::vector<std::string> plotTitles;
  struct gpconfig gpConfig;
  timersettings() : 
    importantLimit(0),weakMode(false),gridMode(false), 
    effMode(false),comparisonMode(false),
    dataColumn(2), plotColumn(3) {};
};

struct pctimervalue {
  double min;
  double max;
  double mean;
  pctimervalue() : min(0), max(0), mean(0) {};
  pctimervalue(const pctimervalue &inVal) : 
    min(inVal.min), max(inVal.max), mean(inVal.mean) {};
};

struct pcruninfo {
  int numproc;
  int numgrids;
  int numthreads;
  int numsteps;
  double totalRuntime;
  size_t ngridpointstotal;
  std::vector<size_t> ngridpoints;
  std::vector<int> ngridproc;
  std::vector<std::vector<size_t> > gridsizes;
  pcruninfo() : numproc(0), numgrids(0), numthreads(0), numsteps(0),totalRuntime(0),ngridpointstotal(0) {};
};

typedef std::map<std::string,struct pctimervalue> pcruntimes;

struct pcrun {
  pcruninfo pcRunInfo;
  pcruntimes pcRunTimes;
};
typedef std::list<std::string> timerlist;
typedef std::map<std::string,timerlist> derivedtimers;

std::string MakePlotTitle(const pcruninfo &pcRunInfo,
			  const timersettings &timerSettings,
			  int mode = 0)
{
  std::ostringstream Ostr;
  int numGrids = pcRunInfo.numgrids;
  size_t numPoints = 0;
  int numProcs = pcRunInfo.numproc;
  if(pcRunInfo.numthreads > 0)
    numProcs *= pcRunInfo.numthreads;
  for(int i = 0;i < numGrids;i++){
    if(pcRunInfo.ngridpoints.size() > numGrids){
      size_t numGridPoints = 1;
      for(int j = i*3;j < i*3+3;j++)
        numGridPoints *= pcRunInfo.ngridpoints[j];
      numPoints += numGridPoints;
    } else {
      numPoints += pcRunInfo.ngridpoints[i];
    }
  }
  if(!timerSettings.plotTitles.empty()){
    if(!timerSettings.plotTitles[mode].empty())
      Ostr << timerSettings.plotTitles[mode];
    return(Ostr.str());
  } else {
    if(!timerSettings.applicationName.empty())
      Ostr << timerSettings.applicationName << " ";
    if(mode == 0){ // scaling title
      Ostr << (timerSettings.weakMode ? "Weak " : "Strong ")
	   << "Scaling";
      if(!timerSettings.platformName.empty())
	Ostr << " on " << timerSettings.platformName;
    } else if(mode == 1){
      Ostr << "Timing ";
      if(numProcs > 0){ // single run title
	Ostr << "on " << numProcs;
	if(!timerSettings.platformName.empty())
	  Ostr << timerSettings.platformName; 
	Ostr << " processors ";
      } else if(!timerSettings.platformName.empty()){
	Ostr << "on " << timerSettings.platformName << " ";
      }
    }
    if(!timerSettings.gridMode){
      if(numGrids > 0){
        Ostr << "(";
        if(numPoints > 0)
          Ostr << numPoints << "points/";
        Ostr << numGrids << "grids)";
      } else if (numPoints > 0){
        Ostr << " (" << numPoints << "grid points) ";
      }
    }
  }
  return(Ostr.str());
}

void SetupGnuPlot(std::ostream &outStream,const timersettings &timerSettings,
		  const std::string &idString,bool toDisplay = false)
{
  if(toDisplay){
    if(!timerSettings.gpConfig.gpDisplayTerm.empty())
      outStream << "set term " << timerSettings.gpConfig.gpDisplayTerm << std::endl;
  } else {
    if(!timerSettings.gpConfig.gpFileTerm.empty())
      outStream << "set term " << timerSettings.gpConfig.gpFileTerm << std::endl;
    if(!timerSettings.gpConfig.gpFileName.empty()){
      outStream << "set output '" << timerSettings.gpConfig.gpFileName
		<< idString;
      if(timerSettings.gpConfig.gpFileTerm == "png")
	outStream << ".png'";
      else if(timerSettings.gpConfig.gpFileTerm == "eps" || 
	      timerSettings.gpConfig.gpFileTerm == "ps")
	outStream << ".ps'";
      else if(timerSettings.gpConfig.gpFileTerm == "jpeg")
	outStream << ".jpg'";
      else if(timerSettings.gpConfig.gpFileTerm == "pdf")
	outStream << ".pdf'";
      outStream << std::endl;
    }
  }
  if(!timerSettings.gpConfig.gpCustomConfig.empty()){
    outStream << timerSettings.gpConfig.gpCustomConfig;
  }
}

void SingleRunPlotCommands(std::ostream &outStream,const std::string &dataFileName,
			   const std::vector<struct pcrun> &pcRuns,int runIndex,
			   const timersettings &timerSettings,bool toDisplay=false)
{
  
  const pcruninfo pcRunInfo(pcRuns[runIndex].pcRunInfo);
  std::string plotTitle(MakePlotTitle(pcRunInfo,timerSettings,1));
  SetupGnuPlot(outStream,timerSettings,"_Timing",toDisplay);
  outStream << "set title '" << plotTitle << "'" << std::endl
	    << "set ylabel 'Time (GigaCycles)'" << std::endl
	    << "set xlabel 'Routine'" << std::endl
	    << "set boxwidth 0.75 absolute" << std::endl
	    << "set style fill solid" << std::endl
	    << "plot '" << dataFileName << "'";
  int i = runIndex;
  outStream << " every 1::1 using " << (i+1)*3 << ":xtic(1) with boxes notitle" << std::endl;
}

void MultiRunPlotCommands(std::ostream &outStream,const std::string &dataFileName,
			  const std::vector<struct pcrun> &pcRuns,const timersettings &timerSettings,
			  bool toDisplay = false)
{
  const pcruninfo pcRunInfo(pcRuns[0].pcRunInfo);
  std::string plotTitle(MakePlotTitle(pcRunInfo,timerSettings));
  int numRuns = pcRuns.size();
  SetupGnuPlot(outStream,timerSettings,"_Timing",toDisplay);
  outStream << "set title '" << plotTitle << "'" << std::endl
	    << "set ylabel 'Time'" << std::endl;
  if(timerSettings.gridMode)
    outStream << "set xlabel 'Number of grid points'" << std::endl;
  else if(timerSettings.comparisonMode)
    outStream << "set xlabel 'Routine Name'" << std::endl;
  else
    outStream << "set xlabel 'Number of PE'" << std::endl;
  outStream << "set boxwidth 0.75 absolute" << std::endl
	    << "set style histogram " 
            << (timerSettings.comparisonMode ? 
                "clustered gap 1" : "columnstack") 
            << std::endl
	    << "set style fill solid" << std::endl
	    << "set style data histogram" << std::endl
	    << "plot '" << dataFileName << "'";
  for(int i = 0;i < numRuns;i++){
    if(i > 0)
      outStream << "''";
    outStream << " using " << (i+1)*3;
    if(i == numRuns - 1)
      outStream << ":key(1) ti col";
    else
      outStream << " ti col,";
  }
  outStream << std::endl;
}

void SpeedUpPlotCommands(std::ostream &outStream,const std::string &dataFileName,
			 const timerlist &timerList,
			 const std::vector<struct pcrun> &pcRuns,const timersettings &timerSettings,
			 bool toDisplay=false,bool linScale=false)
{
  int numTimers = timerList.size();
  const pcruninfo pcRunInfo(pcRuns[0].pcRunInfo);
  int numProcs = pcRunInfo.numproc;
  if(pcRunInfo.numthreads > 0)
    numProcs *= pcRunInfo.numthreads;
  if(timerSettings.gridMode)
    numProcs = pcRunInfo.ngridpointstotal;
  if(timerSettings.effMode){
    SetupGnuPlot(outStream,timerSettings,"_Efficiency",toDisplay);
  } else {
    SetupGnuPlot(outStream,timerSettings,"_Speedup",toDisplay);
  }
  std::string plotTitle(MakePlotTitle(pcRunInfo,timerSettings));
  if(timerSettings.gridMode)
    outStream << "set title 'Grid Scaling on " << timerSettings.platformName
              << "'" << std::endl;
  else
    outStream << "set title '" << plotTitle << "'" << std::endl;
  if(timerSettings.gridMode)
    outStream << "set ylabel 'Normalized Time'" << std::endl;
  else {
    if(timerSettings.effMode){
      outStream << "set ylabel 'Parallel Efficiency'" << std::endl;
    } else {
      outStream << "set ylabel 'Speedup'" << std::endl;
    }
  }
  if(timerSettings.gridMode)
    outStream << "set xlabel 'Number of Grid Points'" << std::endl;
  else
    outStream << "set xlabel 'Number of threads'" << std::endl;
  outStream   << "set key autotitle columnhead" << std::endl
              << "set key left" << std::endl;
  if(timerSettings.gridMode)
    outStream << "f(x) = x/" << numProcs << std::endl;
  else {
    if(timerSettings.effMode){
      outStream << "f(x) = 1" << std::endl;
    } else {
      outStream << "f(x) = x/" << numProcs << std::endl;
    }
  }
  if(!linScale){
    outStream << "set logscale x" << std::endl;
    if(!timerSettings.effMode)
      outStream << "set logscale y" << std::endl; 
  }
  outStream << "plot ";
  //  if(!timerSettings.gridMode)
  outStream   << "f(x) lc 'black' ti 'Ideal',";
  outStream << "'" << dataFileName << "'";
  for(int i = 0;i < numTimers;i++){
    if(i > 0)
      outStream << "''";
    outStream << " using 1:" << i+2 << " w lp";
    if(i != numTimers - 1)
      outStream << ",";
  }
  outStream << std::endl;
}

std::string FormatName(const std::string &inName)
{
  std::string outName(inName);
  std::string::size_type x = outName.find("_");
  while(x != std::string::npos){
    outName.erase(x,1);
    x = outName.find("_");
  }
  return(outName);
}

void SummarizeScalabilityData(std::ostream &outStream,const std::vector<struct pcrun> &pcRuns,
			      const timerlist &timerList, const timersettings &timerSettings)
{

  timerlist::const_iterator timerIt = timerList.begin();
  std::vector<struct pcrun>::const_iterator pcRunIt = pcRuns.begin();
  outStream << "Routine";
  while(timerIt != timerList.end()){
    outStream << "\t" << FormatName(*timerIt++);
  }
  outStream << std::endl;
  std::map<std::string,double> yScales;
  std::map<std::string,int>  yProcs;
  //  double xScale = -1.0;
  while(pcRunIt != pcRuns.end()){
    int runIndex = pcRunIt - pcRuns.begin();
    const pcruninfo &currentRunInfo(pcRunIt->pcRunInfo);
    const pcruntimes &pcRunTimes(pcRunIt++->pcRunTimes);
    int numThreads = currentRunInfo.numproc;
    if(currentRunInfo.numthreads > 0)
      numThreads *= currentRunInfo.numthreads;
    double weakScale = 1.0;
    if(timerSettings.weakMode)
      weakScale = numThreads;
    if(timerSettings.gridMode){
      numThreads = currentRunInfo.ngridpointstotal;
      if(timerSettings.weakMode)
        weakScale = 1.0/(currentRunInfo.ngridpointstotal);
    }
    //    if(xScale < 0.0) xScale = 1.0/((double)numThreads); 
    outStream << numThreads; // << "\t" << xScale*numThreads;
    timerIt = timerList.begin();
    while(timerIt != timerList.end()){
      const std::string &timerName(*timerIt++);
      double yScale = 0.0;
      pctimervalue timerValue;
      if(pcRunTimes.find(timerName) != pcRunTimes.end()){
	timerValue = pcRunTimes.at(timerName);
	std::map<std::string,double>::const_iterator scaleIt = yScales.find(timerName);
	if(scaleIt != yScales.end()){
	  yScale = scaleIt->second;
          float scaleProc = yProcs[timerName];
          if(timerSettings.effMode)
            yScale *= (scaleProc/(float)numThreads);
	} else {
	  yScale = timerValue.max;
          if(yScale == 0){
            std::cout << "WARNING: Found no scale value for " << timerName << std::endl;
            yScale = 1.0;
          }
	  yScales[timerName] = yScale;
          yProcs[timerName]  = numThreads;
	  std::cout << "Setting scale for " << timerName << " = "
		    << yScale <<  " @ " << numThreads << " procs."
                    << std::endl;
	}
        //        weakScale /= yProcs[timerName];
        if(!timerSettings.gridMode){
          outStream << "\t" << yScale*weakScale/timerValue.max;
        } else {
          outStream << "\t" << weakScale*timerValue.max/yScale;
        }
      } else {
        if(!timerSettings.gridMode){
          //          outStream << "\t" << weakScale/yProcs[timerName];
          outStream << "\t" << weakScale;
        } else {
          outStream << "\t1";
        }
      }
    }
    outStream << std::endl;
  }
}

void SummarizeTimingData(std::ostream &outStream,const std::vector<struct pcrun> &pcRuns,
			 const timerlist &timerList,const timersettings &timerSettings)
{
  timerlist::const_iterator timerIt = timerList.begin();
  std::vector<struct pcrun>::const_iterator pcRunIt = pcRuns.begin();
  outStream << "Routine";
  while(pcRunIt != pcRuns.end()){
    int numProc = pcRunIt->pcRunInfo.numproc;
    if(pcRunIt->pcRunInfo.numthreads > 0)
      numProc *= pcRunIt->pcRunInfo.numthreads;
    if(timerSettings.gridMode)
      numProc = pcRunIt->pcRunInfo.ngridpointstotal;
    outStream << "\t" << numProc << "\t" << numProc << "\t" << numProc; 
    pcRunIt++;
  }
  outStream << std::endl;
  while(timerIt != timerList.end()){
    const std::string &timerName(*timerIt++);
    outStream << FormatName(timerName);
    pcRunIt = pcRuns.begin();
    while(pcRunIt != pcRuns.end()){
      const pcruntimes &pcRunTimes(pcRunIt++->pcRunTimes);
      pctimervalue timerValue;
      if(pcRunTimes.find(timerName) != pcRunTimes.end()){
	timerValue = pcRunTimes.at(timerName);
      } 
      outStream << "\t" << timerValue.min << "\t" << timerValue.max << "\t" << timerValue.mean;
    }
    outStream << std::endl;
  }
}

bool TimerIncluded(const std::string &routineName,const timerlist &timerList)
{
  timerlist::const_iterator listIt = timerList.begin();
  while(listIt != timerList.end()){
    std::string timerPattern(*listIt++);
    if(timerPattern == "*")
      return(true);
    if(timerPattern[0] == '-')
      timerPattern = timerPattern.substr(1);
    if(timerPattern == routineName)
      return(true);
    //    if(routineName.find(timerPattern) != std::string::npos)
  }
  return(false);
}

int GetDerivedOperationForRoutine(const std::string &timerName,const std::string &routineName,const derivedtimers &derivedTimers)
{
  derivedtimers::const_iterator derivedIt = derivedTimers.begin();
  bool found = false;
  timerlist derivedTimerNames;
  while(derivedIt != derivedTimers.end() && !found){
    const std::string &derivedTimerName(derivedIt->first);
    if(timerName == derivedTimerName){
      found = true;
      derivedTimerNames = derivedIt->second;
    } else {
      derivedIt++;
    }
  }
  if(!found)
    return(-1);
  timerlist::iterator listIt = derivedTimerNames.begin();
  while(listIt != derivedTimerNames.end()){
    std::string &timerPattern(*listIt++);
    int op = 0;
    if(timerPattern[0] == '-'){
      timerPattern = timerPattern.substr(1);
      op = 1;
    }
    if(routineName == timerPattern)
      return(op);
  }
  return(-1);
}

std::vector<std::string> GetDerivedTimerNames(const std::string &routineName,const derivedtimers &derivedTimers)
{
  std::vector<std::string> derivedTimerNames;
  derivedtimers::const_iterator derivedIt = derivedTimers.begin();
  std::string emptyString;
  while(derivedIt != derivedTimers.end()){
    const std::string &derivedTimerName(derivedIt->first);
    const timerlist &timerNames(derivedIt++->second);
    if(TimerIncluded(routineName,timerNames))
      derivedTimerNames.push_back(derivedTimerName);
  }
  return(derivedTimerNames);
}

pcruntimes ProcessRun(const pcruninfo &pcRunInfo,const pcruntimes &rawRunTimes,const derivedtimers &derivedTimers,
		      const timerlist &excludedTimers,const timersettings &timerSettings,
		      pctimervalue &timingDelta)
{
  pcruntimes processedTimers;
  pctimervalue timedTotal;
  pctimervalue pcTotal;
  pcruntimes::const_iterator totalTimeIt;
  if(!timerSettings.totalTimer.empty()){
    totalTimeIt = rawRunTimes.find(timerSettings.totalTimer);
    if(totalTimeIt != rawRunTimes.end()){
      pcTotal = rawRunTimes.at(timerSettings.totalTimer);
    }
  }
  if(pcTotal.max <= 0.0){
    totalTimeIt = rawRunTimes.find(timerSettings.applicationName);
    if(totalTimeIt != rawRunTimes.end()){
      pcTotal = rawRunTimes.at(timerSettings.applicationName);
    }
  }
  if(pcTotal.max <= 0.0){
    //   std::cout << "Setting pcTotal runtime to " << pcRunInfo.totalRuntime << std::endl;
    pcTotal.max  = pcRunInfo.totalRuntime;
    pcTotal.min  = pcRunInfo.totalRuntime;
    pcTotal.mean = pcRunInfo.totalRuntime;
  }
  assert(pcTotal.max > 0.0);
  //  const pctimervalue &pcTotal(rawRunTimes.at("PlasComCM"));
  pcruntimes::const_iterator rawRunTimesIt = rawRunTimes.begin();
  while(rawRunTimesIt != rawRunTimes.end()){
    const std::string &routineName(rawRunTimesIt->first);
    const pctimervalue &timerValue(rawRunTimesIt++->second);
    std::vector<std::string> timerNames(GetDerivedTimerNames(routineName,derivedTimers));
    if(timerNames.empty()){
      if(TimerIncluded(routineName,excludedTimers))
	continue;
      if(timerValue.max/pcTotal.max >= timerSettings.importantLimit){
	timerNames.push_back(routineName);
      } else {
	timerNames.push_back("Other");
      }
    }
    timedTotal.min    += timerValue.min;
    timedTotal.max    += timerValue.max;
    timedTotal.mean   += timerValue.mean;
    std::vector<std::string>::iterator timerNameIt = timerNames.begin();
    while(timerNameIt != timerNames.end()){
      std::string &timerName(*timerNameIt++);
      //      std::cout << "Found timer name " << timerName << " for " << routineName << std::endl;
      pctimervalue &pcTimerValue(processedTimers[timerName]);
      int op = GetDerivedOperationForRoutine(timerName,routineName,derivedTimers);
      if(op == 0){
        pcTimerValue.min  += timerValue.min;
        pcTimerValue.max  += timerValue.max;
        pcTimerValue.mean += timerValue.mean;
      } else if(op == 1){
        pcTimerValue.min  -= timerValue.min;
        pcTimerValue.max  -= timerValue.max;
        pcTimerValue.mean -= timerValue.mean;
      } else {
        std::cerr << "ERROR: Undefined operation for derived timer/routine = "
                  << timerName << "/" << routineName << std::endl;
        exit(1);
      }
    }
  }
  timingDelta.min  = pcTotal.min  - timedTotal.min;
  timingDelta.max  = pcTotal.max  - timedTotal.max;
  timingDelta.mean = pcTotal.mean - timedTotal.mean;
 
  return(processedTimers);
}

void ReadTimers(pcruntimes &routineTimes,
		std::ifstream &inFile,int dataColumn)
{
  std::string line;
  while(std::getline(inFile,line)){
    if(line.empty())
      continue;
    if(line[0] == '#')
      continue;
    std::string routineName;
    pctimervalue pcTimerValue;
    std::istringstream Istr(line);
    Istr >> routineName >> pcTimerValue.min >> pcTimerValue.max >> pcTimerValue.mean;
    if(dataColumn == 1){
      double holdVal = pcTimerValue.min;
      pcTimerValue.min = pcTimerValue.max;
      pcTimerValue.max = holdVal;
    }
    routineTimes[routineName] = pcTimerValue;
  }
}
void SetConfigurationDefaults(ConfigType &inConfig)
{
  inConfig.push_back(std::make_pair(std::string("DerivedTimer::RHS"),std::string("rhs_total")));
  inConfig.push_back(std::make_pair(std::string("DerivedTimer::ReadInterp"),std::string("interp_setup")));
  inConfig.push_back(std::make_pair(std::string("DerivedTimer::ReadGrids"),std::string("InitGrids")));
  inConfig.push_back(std::make_pair(std::string("DerivedTimer::ReadState"),std::string("InitState")));
  inConfig.push_back(std::make_pair(std::string("DerivedTimer::Interpolation"),std::string("interp_vol")));
  inConfig.push_back(std::make_pair(std::string("DerivedTimer::Write"),std::string("io_write_restart io_write_soln")));
  inConfig.push_back(std::make_pair(std::string("TimerSetting::ImportantLimit"),std::string("0.05")));
  inConfig.push_back(std::make_pair(std::string("DerivedTimer::Names"),
				    std::string("RHS Interpolation ReadGrids ReadState ReadInterp Write"))); 
  inConfig.push_back(std::make_pair(std::string("ExcludedTimer::Names"),
				    std::string("PlasComCM RunTime MainLoop RungeKutta SpecificRKTime operator rhs io_total InitTotal")));
}

void PopulateRunInfo(pcruninfo &pcRunInfo,std::ifstream &inFile)
{
  pcRunInfo.numproc = 0;
  pcRunInfo.numgrids = 0;
  pcRunInfo.numsteps = 0;
  inFile.seekg(0);
  std::string line;
  while(line.empty() && inFile)
    std::getline(inFile,line);
  int nline = 0;
  while(line[0] == '#'){
    int streamPos = inFile.tellg();
    if(nline > 0){
      std::getline(inFile,line);
      while(line.empty() && inFile)
	std::getline(inFile,line);
    }
    if(line[0] == '#'){
      nline++;
      std::string::size_type y = line.find(":");
      if(y == std::string::npos)
	continue;
      std::istringstream Istr(line.substr(y+1));
      if(line.find("Number of procs:") != std::string::npos){
	Istr >> pcRunInfo.numproc;
      } else if (line.find("Number of grids:") != std::string::npos) {
	Istr >> pcRunInfo.numgrids;
      } else if (line.find("Number of threads:") != std::string::npos) {
	Istr >> pcRunInfo.numthreads;
      } else if (line.find("Number of steps:") != std::string::npos){
	Istr >> pcRunInfo.numsteps;
      } else if (line.find("Number of points:") != std::string::npos){
	size_t npoints = 0;
        size_t npointstotal = 1;
	while(Istr >> npoints){
	  if(npoints > 0){
	    pcRunInfo.ngridpoints.push_back(npoints);
            npointstotal *= npoints;
          }
	}
        pcRunInfo.ngridpointstotal = npointstotal;
        std::cout << "Num grid points total: " << pcRunInfo.ngridpointstotal << std::endl;
      } else if (line.find("Procs per grid:") != std::string::npos){
	int nprocs = 0;
	while(Istr >> nprocs){
	  if(nprocs > 0)
	    pcRunInfo.ngridproc.push_back(nprocs);
	}
      } else if (line.find("Total Execution Time:") != std::string::npos){
	Istr >> pcRunInfo.totalRuntime;
      }
    } else {
      inFile.seekg(streamPos);
    }
  }
  if(pcRunInfo.numproc == 0)
    pcRunInfo.numproc = 1;
  if(pcRunInfo.numgrids == 0)
    pcRunInfo.numgrids = 1;
  if(pcRunInfo.numthreads == 0)
    pcRunInfo.numthreads = 1;
}

void SetupTimerSettings(timersettings &timerSettings,
			const ConfigType &inConfig)
{
  const std::string settingKey("TimerSetting::");
  timerSettings.importantLimit = 0.05;
  if(inConfig.IsSet(settingKey+"ImportantLimit"))
    timerSettings.importantLimit = inConfig.GetValue<double>(settingKey+"ImportantLimit");
  if(inConfig.IsSet(settingKey+"WeakMode"))
    timerSettings.weakMode = inConfig.GetFlagValue(settingKey+"WeakMode");
  if(inConfig.IsSet(settingKey+"GridMode"))
    timerSettings.gridMode = inConfig.GetFlagValue(settingKey+"GridMode");
  if(inConfig.IsSet(settingKey+"ComparisonMode"))
    timerSettings.gridMode = inConfig.GetFlagValue(settingKey+"ComparisonMode");
  if(inConfig.IsSet(settingKey+"PlatformName"))
    timerSettings.platformName = inConfig.GetValue(settingKey+"PlatformName");
  if(inConfig.IsSet(settingKey+"ApplicationName"))
    timerSettings.applicationName = inConfig.GetValue(settingKey+"ApplicationName");
  if(inConfig.IsSet(settingKey+"DataColumn"))
    timerSettings.dataColumn = inConfig.GetValue<int>(settingKey+"DataColumn");
  const std::string gpKey("GNUPlot::");
  if(inConfig.IsSet(gpKey+"DisplayTerm"))
    timerSettings.gpConfig.gpDisplayTerm = inConfig.GetValue(gpKey+"DisplayTerm");
  if(inConfig.IsSet(gpKey+"FileTerm"))
    timerSettings.gpConfig.gpFileTerm = inConfig.GetValue(gpKey+"FileTerm");
  if(inConfig.IsSet(gpKey+"FileName"))
    timerSettings.gpConfig.gpFileName = inConfig.GetValue(gpKey+"FileName");
  const std::string gpConfigKey(gpKey+"CustomConfig::");
  if(inConfig.IsSet(gpConfigKey+"Names")){
    std::ostringstream customConfigStream;
    std::vector<std::string> customConfigNames(inConfig.GetValueVector(gpConfigKey+"Names"));
    std::vector<std::string>::iterator configIt = customConfigNames.begin();
    while(configIt != customConfigNames.end()){
      std::string &configName(*configIt++);
      std::string configKey(gpConfigKey+configName);
      if(inConfig.IsSet(configKey)){
	customConfigStream << inConfig.Param(configKey) << std::endl;
      }
    }
    timerSettings.gpConfig.gpCustomConfig = customConfigStream.str();
  }
}


void SetupExcludedTimers(timerlist &excludedTimers,
			 const ConfigType &inConfig)
{
  excludedTimers = inConfig.GetValueList("ExcludedTimer::Names");
}

void SetupDerivedTimers(derivedtimers &derivedTimers,
			const ConfigType &inConfig)
{
  const std::string configKey("DerivedTimer::");
  timerlist derivedTimerNames(inConfig.GetValueList("DerivedTimer::Names"));
  timerlist::iterator timerNameIt = derivedTimerNames.begin();
  while(timerNameIt != derivedTimerNames.end()){
    std::string &timerName(*timerNameIt++);
    std::string timerKey(configKey+timerName);
    timerlist &derivedTimerLabels(derivedTimers[timerName]);
    derivedTimerLabels = inConfig.GetValueList(timerKey);
  }
}
